# Descomplicando Gitlab-LINUXtips

Treinamento do LINUXtips na twitch.tv sobre descomplicando gitlab

### Dia 1: Entendemos o que é o git 
```bash
  - Entendemos o que é o gitlab
  - Aprendemos os comandos basicos para manipulação de arquivos e diretórios no git 
  - Como criar uma branch 
  - Como criar um Merge Request
  - Como criar um repositório git
  - Como adicionar um membro no projeto
  - Como criar um grupo no gitlab 
  - Como fazer o merge na master/main
  ```